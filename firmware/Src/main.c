/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f7xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdbool.h>

#include "usbd_cdc_if.h"
#include "po6030k.h"
#include "doge.h"
#include "filters.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

DCMI_HandleTypeDef hdcmi;
DMA_HandleTypeDef hdma_dcmi;

I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
#define SDRAM_ADDRESS 0xC0000000
uint32_t* p_sdram = (uint32_t*) SDRAM_ADDRESS;

uint8_t frame_buffer[320*240 + 1024];

uint8_t USB_rx_flag = 0;
uint8_t USB_rx_buff[32];

uint8_t USB_tx_buff[4096];

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_DCMI_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void debug_print(char *format, uint32_t value);
void USB_data_receive(void);
void command_parse(char* command_str);
void send_qvga_frame(uint8_t *address);
void move_image_to_FB(uint8_t *p_image, uint32_t size);
void filter_image_2D(uint8_t *p_image, filter *p_filter);
void filter_image_1Dv1(uint8_t *p_image, filter *p_filter);
void test_multiply_vs_shift(void);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_DCMI_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
	HAL_Delay(1000);																							// odczekanie na polaczenie USB
	
	debug_print("Program start\r\n", NULL);
	debug_print("System clock %d MHz\r\n", SystemCoreClock/1000000);
			
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4,  GPIO_PIN_RESET);				// TESTEN?
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2,  GPIO_PIN_RESET);				// TESTEN?
	
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7,  GPIO_PIN_RESET);				// reset
	for(int i = 0; i < 32; i++) 
		__NOP();
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7,  GPIO_PIN_SET);					// niereset
	//HAL_GPIO_WritePin(GPIOH, GPIO_PIN_13, GPIO_PIN_SET);			  	// PWREN - jednak nie
	
	po6030k_assign_i2c(&hi2c1);
	uint16_t po6030k_id = po6030k_get_ID();
	debug_print("PO6030K ID = 0x%04X\r\n", po6030k_id);
	if(po6030k_id == PO6030K_REG_VAL_ID)
		debug_print("PO6030K OK\r\n", NULL);
	else
		debug_print("PO6030K not detected!\r\n", NULL);
	
	// camera config
	//po6030k_config_cam(160, 120, 320, 240, 1, 1, PO6030K_MODE_GRAY);			// 320x240 window from P(160, 120) (middle), 1 x and y zoom, grayscale
	po6030k_init_cam(false);
	//HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_CONTINUOUS, (uint32_t) frame_buffer, 320*240);			// nie zadziala bez frame eventa
		
	// enable debug core cycle counter
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
			
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	
	debug_print("Entering main program loop\r\n", NULL);
		
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		USB_data_receive();
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Activate the Over-Drive mode 
    */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48SOURCE_PLL;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* DCMI init function */
static void MX_DCMI_Init(void)
{

  hdcmi.Instance = DCMI;
  hdcmi.Init.SynchroMode = DCMI_SYNCHRO_HARDWARE;
  hdcmi.Init.PCKPolarity = DCMI_PCKPOLARITY_FALLING;
  hdcmi.Init.VSPolarity = DCMI_VSPOLARITY_LOW;
  hdcmi.Init.HSPolarity = DCMI_HSPOLARITY_LOW;
  hdcmi.Init.CaptureRate = DCMI_CR_ALL_FRAME;
  hdcmi.Init.ExtendedDataMode = DCMI_EXTEND_DATA_8B;
  hdcmi.Init.JPEGMode = DCMI_JPEG_DISABLE;
  hdcmi.Init.ByteSelectMode = DCMI_BSM_ALL;
  hdcmi.Init.ByteSelectStart = DCMI_OEBS_ODD;
  hdcmi.Init.LineSelectMode = DCMI_LSM_ALL;
  hdcmi.Init.LineSelectStart = DCMI_OELS_ODD;
  if (HAL_DCMI_Init(&hdcmi) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0xF000F3FF;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Analogue filter 
    */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_DISABLE) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Digital filter 
    */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
static void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream1_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, DCMI_NRST_Pin|DCMI_GPIO_1_Pin|DCMI_GPIO_2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(DCMI_PWR_EN_GPIO_Port, DCMI_PWR_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DCMI_NRST_Pin DCMI_GPIO_1_Pin DCMI_GPIO_2_Pin */
  GPIO_InitStruct.Pin = DCMI_NRST_Pin|DCMI_GPIO_1_Pin|DCMI_GPIO_2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : DCMI_PWR_EN_Pin */
  GPIO_InitStruct.Pin = DCMI_PWR_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(DCMI_PWR_EN_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void debug_print(char *format, uint32_t value)
{
	uint8_t uart_buff[64];
	uint8_t length;
	memset(uart_buff, 0, sizeof(uart_buff));
	length = sprintf((char*) uart_buff, format, value);
	//HAL_UART_Transmit(&huart2, uart_buff, sizeof(uart_buff), 10000);
	while(CDC_Transmit_FS(uart_buff, length) != USBD_OK);
}

void data_transmit(uint8_t *data, uint32_t length)
{
	CDC_Transmit_FS(data, length);
}

void USB_data_receive()
{
	if(USB_rx_flag != 0)
	{
		USB_rx_flag = 0;
		
		/* potwierdzenie odbioru danych, do wywalenia p�zniej - ok, dziala
		uint8_t buff[64];
		uint8_t length;
		memset(buff, 0, sizeof(buff));
		length = sprintf((char*) buff, "Odebrano dane: %s\r\n", data_received);
		CDC_Transmit_FS(buff, length);*/
		
		char preamble[4];																	
		memset(preamble, 0, sizeof(preamble));
		memcpy(preamble, USB_rx_buff, 4);								// z uint8_t na char ale to nie robi
		
		// sprawdz preambule
		// CMD - komenda
		
		if(!memcmp(preamble, (char*) "CMD:", 4))
		{
			char command[32-4];
			memcpy(command, USB_rx_buff+4, 32-4);
			command_parse(command);
		}
		
		// PNG - ping		
		if(!memcmp(preamble, (char*) "PING", 4))
		{
			debug_print("PING OK\r\n", NULL);
		}
		
		memset(USB_rx_buff, 0, sizeof(USB_rx_buff));
	}
}

void command_parse(char* command_str)
{
	//debug_print("Otrzymano komende ", NULL);
	//debug_print(command_str, NULL);
	//debug_print("\r\n", NULL);
	
	if(!memcmp(command_str, "TESTBAND", 8))
	{
		for(uint8_t i=0; i<16; i++)
		{
			memset(USB_tx_buff, 'A'+i, sizeof(USB_tx_buff));	
			while(CDC_Transmit_FS(USB_tx_buff, sizeof(USB_tx_buff)) != USBD_OK);
			HAL_Delay(5);							// father forgive me
		}
	}
	
	if(!memcmp(command_str, "TESTIMG", 7))
	{
		move_image_to_FB((uint8_t*) doge, 320*240);
		send_qvga_frame((uint8_t*) frame_buffer);		
	}
	
	if(!memcmp(command_str, "TESTSNAP", 8))
	{
		HAL_DCMI_Start_DMA(&hdcmi, DCMI_MODE_SNAPSHOT, (uint32_t) frame_buffer, 320*240);
		//while(!(hdcmi.State == HAL_DCMI_STATE_READY));		// nie dziala
		HAL_Delay(10);																			// transmisja ramki zajmuje ok 4 ms, wiec jest duzy zapas
		HAL_DCMI_Stop(&hdcmi);
		send_qvga_frame((uint8_t*) frame_buffer);
	}
	
	if(!memcmp(command_str, "TESTMULT", 8))
	{
		test_multiply_vs_shift();
	}
	
	if(!memcmp(command_str, "TESTFILT", 8))
	{
		filter* p_filter;
		
		// check filter type
		switch(command_str[8]) 
		{
			case '1':
				p_filter = (filter*) &filter_mean_3;
				break;
			case '2':
				p_filter = (filter*) &filter_mean_5;
				break;
			case '3':
				p_filter = (filter*) &filter_gauss_3;
				break;
			case '4':
				p_filter = (filter*) &filter_gauss_5;
				break;
			case '5':
				p_filter = (filter*) &filter_sharpen;
				break;
			case '6':
				p_filter = (filter*) &filter_edge;
				break;
			case '7':
				p_filter = (filter*) &filter_sobel_x;
				break;
			case '8':
				p_filter = (filter*) &filter_sobel_y;
				break;
			default:
				break;
		}
		
		// check algorithm type
		switch(command_str[9]) 
		{
			case '1':
				filter_image_2D((uint8_t*) doge, p_filter);
				break;
			case '2':
				filter_image_1Dv1((uint8_t*) doge, p_filter);
				break;
			case '3':
				break;
			default:
				break;
		}
		
		// send filtered image
		send_qvga_frame((uint8_t*) frame_buffer);
	}		
}

void send_qvga_frame(uint8_t *address)
{
	// transmit full 4096 byte packages
	for(uint32_t i=0; i<19; i++)
	{
		while(CDC_Transmit_FS((uint8_t*) (address + 4096*i), 4096) != USBD_OK);
		HAL_Delay(5);							
	}			
}

void move_image_to_FB(uint8_t *p_image, uint32_t size)
{
	memcpy(frame_buffer, p_image, size);
}

void filter_image_2D(uint8_t *p_image, filter *p_filter)
{
	// save cycles counter state at start
	__disable_irq();
	uint32_t counter_start = DWT->CYCCNT;
	
	uint32_t margin = (p_filter->size)/2;
	
	// 2D filtration 
	for(uint32_t row=margin; row<240-margin; row++)
	{
		for(uint32_t col=margin; col<320-margin; col++)
		{
			uint32_t pixel_index = (row*320) + col;
			int      sum = 0;
			
			// sumowanie macierz * piksele
			for(int i=-margin; i<=(int) margin; i++)
			{
				for(int j=-margin; j<=(int) margin; j++)
				{
					uint32_t filter_index = (i+margin)*p_filter->size + (j+margin);
					sum += p_filter->kernel[filter_index] * ((int) p_image[pixel_index + (i*320) + j]);
				}
			}
			
			sum /= p_filter->divider;
			frame_buffer[pixel_index] = (uint8_t) (sum & 0xFF);
		}
	}
	
	// calculate cycles taken
	uint32_t counter_stop = DWT->CYCCNT;
	__enable_irq();
	debug_print("RESP:%d\r\n", counter_stop-counter_start);
}

void filter_image_1Dv1(uint8_t *p_image, filter *p_filter)
{	
	// save cycles counter state at start
	__disable_irq();
	uint32_t counter_start = DWT->CYCCNT;
	// unrolled filtration loops for maximum speed
	// perhaps using math coprocessor instructions could be even more effective
	// 3x3 filter size
	if(p_filter->size == 3)
	{
		for(int pixel_index = 76478; pixel_index > 321; pixel_index--)
		{
			int sum = p_filter->kernel[0] * p_image[pixel_index - 321];
			sum += p_filter->kernel[1] * p_image[pixel_index - 320];
			sum += p_filter->kernel[2] * p_image[pixel_index - 319];
			sum += p_filter->kernel[3] * p_image[pixel_index - 1];
			sum += p_filter->kernel[4] * p_image[pixel_index];
			sum += p_filter->kernel[5] * p_image[pixel_index + 1];
			sum += p_filter->kernel[6] * p_image[pixel_index + 319];
			sum += p_filter->kernel[7] * p_image[pixel_index + 320];
			sum += p_filter->kernel[8] * p_image[pixel_index + 321];
			
			sum /= p_filter->divider;
			frame_buffer[pixel_index] = sum;
		}
	}
	
	// 5x5 filter size
	if(p_filter->size == 5)
	{
		for(int pixel_index = 76157; pixel_index > 642; pixel_index--)
		{
			int sum = p_filter->kernel[0] * p_image[pixel_index - 642];
			sum += p_filter->kernel[1]  * p_image[pixel_index - 641];
			sum += p_filter->kernel[2]  * p_image[pixel_index - 640];
			sum += p_filter->kernel[3]  * p_image[pixel_index - 639];
			sum += p_filter->kernel[4]  * p_image[pixel_index - 638];
			sum += p_filter->kernel[5]  * p_image[pixel_index - 322];
			sum += p_filter->kernel[6]  * p_image[pixel_index - 321];
			sum += p_filter->kernel[7]  * p_image[pixel_index - 320];
			sum += p_filter->kernel[8]  * p_image[pixel_index - 319];
			sum += p_filter->kernel[9]  * p_image[pixel_index - 318];
			sum += p_filter->kernel[10] * p_image[pixel_index - 2];
			sum += p_filter->kernel[11] * p_image[pixel_index - 1];
			sum += p_filter->kernel[12] * p_image[pixel_index];
			sum += p_filter->kernel[13] * p_image[pixel_index + 1];
			sum += p_filter->kernel[14] * p_image[pixel_index + 2];
			sum += p_filter->kernel[15] * p_image[pixel_index + 318];
			sum += p_filter->kernel[16] * p_image[pixel_index + 319];
			sum += p_filter->kernel[17] * p_image[pixel_index + 320];
			sum += p_filter->kernel[18] * p_image[pixel_index + 321];
			sum += p_filter->kernel[19] * p_image[pixel_index + 322];
			sum += p_filter->kernel[20] * p_image[pixel_index + 638];
			sum += p_filter->kernel[21] * p_image[pixel_index + 639];
			sum += p_filter->kernel[22] * p_image[pixel_index + 640];
			sum += p_filter->kernel[23] * p_image[pixel_index + 641];
			sum += p_filter->kernel[24] * p_image[pixel_index + 642];
		
			sum /= p_filter->divider;
			frame_buffer[pixel_index] = sum;
		}
	}
	
	// old shitty algorithm 
	/*for(uint32_t row=margin; row<height-margin; row++)
	{
		for(uint32_t col=margin; col<width-margin; col++)
		{
			uint32_t pixel_index = (row*width) + col;
			int      sum = 0;
			
			// sumowanie macierz * piksele
			for(int i=-margin; i<=(int) margin; i++)
			{
				for(int j=-margin; j<=(int) margin; j++)
				{
					uint32_t filter_index = (i+margin)*p_filter->size + (j+margin);
					sum += p_filter->kernel[filter_index] * ((int) p_image[pixel_index + (i*width) + j]);
				}
			}
			
			sum /= p_filter->divider;
			frame_buffer[pixel_index] = (uint8_t) (sum & 0xFF);
		}
	}*/
	
	// calculate cycles taken
	uint32_t counter_stop = DWT->CYCCNT;
	__enable_irq();
	debug_print("RESP:%d\r\n", counter_stop-counter_start);
}

void test_multiply_vs_shift()
{
	uint32_t counter_start;
	uint32_t counter_stop;
	uint32_t i;
	uint8_t *p_test_buffer_8b = calloc(sizeof(uint8_t), 1024);
	if(p_test_buffer_8b == NULL)
	{
		debug_print("RESP:0\r\n", NULL);
		debug_print("RESP:0\r\n", NULL);
	}
	else
	{
		// pomiar czasu mnozenia x2
		memset(p_test_buffer_8b, 13, 1024);
		__disable_irq();
		counter_start = DWT->CYCCNT;
		for(i=0; i<1024; i++)
		{
			*(p_test_buffer_8b+i) *= 2;
		}
		counter_stop = DWT->CYCCNT;
		__enable_irq();
		debug_print("RESP:%d\r\n", counter_stop-counter_start);
		
		// pomiar czasu przesuniecia bitowego <<1
		memset(p_test_buffer_8b, 13, 1024);
		__disable_irq();
		counter_start = DWT->CYCCNT;
		for(i=0; i<1024; i++)
		{
			*(p_test_buffer_8b+i) <<= 1;
		}
		counter_stop = DWT->CYCCNT;
		__enable_irq();
		debug_print("RESP:%d\r\n", counter_stop-counter_start);
	}
	
}

void HAL_DCMI_ErrorCallback(DCMI_HandleTypeDef *hdcmi)
{
	//debug_print("DCMI error interrupt\r\n", NULL);
}

void HAL_DCMI_LineEventCallback(DCMI_HandleTypeDef *hdcmi)
{
	//debug_print("DCMI line event interrupt\r\n", NULL);
}

void HAL_DCMI_VsyncEventCallback(DCMI_HandleTypeDef *hdcmi)
{
	//debug_print("DCMI vsynch event interrupt\r\n", NULL);
}

void HAL_DCMI_FrameEventCallback(DCMI_HandleTypeDef *hdcmi)
{
	//debug_print("DCMI frame event interrupt\r\n", NULL);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
