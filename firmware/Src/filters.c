#include "filters.h"

// macierze filtrow konwolucyjnych
const filter filter_mean_3 = 
{
	.size = 3,
	.divider = 9,
	.kernel = 
	{
		1, 1, 1, 
		1, 1, 1, 
		1, 1, 1
	}
};

const filter filter_mean_5 = 
{
	.size = 5,
	.divider = 25,
	.kernel = 
	{
		1, 1, 1, 1, 1, 
		1, 1, 1, 1, 1, 
		1, 1, 1, 1, 1, 
		1, 1, 1, 1, 1, 
		1, 1, 1, 1, 1
	}
};

const filter filter_gauss_3 = 
{
	.size = 3,
	.divider = 16,
	.kernel = 
	{
		1, 2, 1,
		2, 4, 2,
		1, 2, 1
	}
};

const filter filter_gauss_5 = 
{
	.size = 5,
	.divider = 273,
	.kernel = 
	{
		1,  4,  7,  4,  1, 
		4, 16, 26, 16,  4,
		7, 26, 41, 26,  7,
		4, 16, 26, 16,  4,
		1,  4,  7,  4,  1
	}
};

const filter filter_edge = 
{
	.size = 3,
	.divider = 1,
	.kernel = 
	{
		1,  1,  1,
		1, -8,  1,
		1,  1,  1
	}
};

const filter filter_sharpen = 
{
	.size = 3,
	.divider = 1,
	.kernel = 
	{
		 0, -1,  0,
		-1,  5, -1,
		 0, -1,  0
	}
};

const filter filter_sobel_x = 
{
	.size = 3,
	.divider = 1,
	.kernel = 
	{
		1, 0, -1,
		2, 0, -2,
		1, 0, -1
	}
};

const filter filter_sobel_y = 
{
	.size = 3,
	.divider = 1,
	.kernel = 
	{
		 1,  2,  1,
		 0,  0,  0,
		-1, -2, -1
	}
};
