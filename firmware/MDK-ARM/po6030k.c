// bleble nagl�wek costam Marcin Rybak 2017


#include "po6030k.h"

static I2C_HandleTypeDef *i2c;
extern void debug_print(char *format, uint32_t value);
uint8_t current_bank = 0;



// Assign I2C handler to function, so it doesn't have to be passed with every function
void po6030k_assign_i2c(I2C_HandleTypeDef *hi2c)
{
	i2c = hi2c;
}



// Get camera ID (should be 0x6030), return it or 0 if error.
uint16_t po6030k_get_ID()
{
	uint8_t data[2];
	HAL_I2C_Mem_Read(i2c, PO6030K_SLAVE_ADDR, PO6030K_REG_ADDR_ID_H, 1, data, 2, 10000);
	return (data[1] | data[0]<<8);
}



// Set register bank
void po6030k_set_bank(uint8_t bank) 
{
	if(bank != current_bank)
	{
		do
		{
			HAL_I2C_Mem_Write(i2c, PO6030K_SLAVE_ADDR, PO6030K_REG_BANK, 1, &bank, 1, 10000);
			HAL_Delay(10);
		}
		while(po6030k_quick_read(0x03) != bank);
	}
}



// Write to a register
void po6030k_write_register(uint8_t bank, uint8_t reg, uint8_t value) {
	do
	{
		po6030k_set_bank(bank);
		HAL_I2C_Mem_Write(i2c, PO6030K_SLAVE_ADDR, reg, 1, &value, 1, 10000);
		HAL_Delay(10);
	}
	while(po6030k_quick_read(reg) != value);
	
	debug_print("   Set value at address 0x%02X ", reg);
	debug_print("in bank 0x%02X ", po6030k_quick_read(0x03));
	debug_print("to 0x%02X\r\n", po6030k_quick_read(reg));
}



// Read register value in a specific bank
uint8_t po6030k_read_register(uint8_t bank, uint8_t reg) {
	uint8_t data;
	po6030k_set_bank(bank);
	HAL_I2C_Mem_Read(i2c, PO6030K_SLAVE_ADDR, reg, 1, &data, 1, 10000);
	
	return data;
}



// Quick read address regardless of bank
uint8_t po6030k_quick_read(uint8_t reg) {
	uint8_t data;
	HAL_I2C_Mem_Read(i2c, PO6030K_SLAVE_ADDR, reg, 1, &data, 1, 10000);
	
	return data;
}



// ???
void po6030k_set_bayer_clkdiv(uint8_t div) 
{
	po6030k_write_register(PO6030K_BANK_A, 0x91, div);
}
	


// ???
void po6030k_set_pclkdiv(uint8_t div) 
{
	po6030k_write_register(PO6030K_BANK_B, 0x68, div);
}



// initialize camera
void po6030k_init_cam(bool testmode)
{	
	// QVGA settings according to datasheet p60
	debug_print("PO6030K resolution config (QVGA):\r\n", NULL);
	po6030k_write_register(PO6030K_BANK_B, 0x51, 0x04);
	po6030k_write_register(PO6030K_BANK_B, 0x53, 0x04);
	po6030k_write_register(PO6030K_BANK_B, 0x54, 0x01);
	po6030k_write_register(PO6030K_BANK_B, 0x55, 0x43);
	po6030k_write_register(PO6030K_BANK_B, 0x56, 0x00);
	po6030k_write_register(PO6030K_BANK_B, 0x57, 0xF3);
	po6030k_write_register(PO6030K_BANK_B, 0x61, 0x0C);
	po6030k_write_register(PO6030K_BANK_B, 0x63, 0xEC);
	po6030k_write_register(PO6030K_BANK_B, 0x80, 0x40);
	po6030k_write_register(PO6030K_BANK_B, 0x81, 0x40);
	po6030k_write_register(PO6030K_BANK_B, 0x82, 0x01);
	po6030k_write_register(PO6030K_BANK_C, 0x11, 0x12);
	po6030k_write_register(PO6030K_BANK_C, 0x13, 0x0E);
	po6030k_write_register(PO6030K_BANK_C, 0x14, 0x01);
	po6030k_write_register(PO6030K_BANK_C, 0x15, 0x60);
	po6030k_write_register(PO6030K_BANK_C, 0x16, 0x00);
	po6030k_write_register(PO6030K_BANK_C, 0x17, 0xDF);
	po6030k_write_register(PO6030K_BANK_C, 0x19, 0x72);
	po6030k_write_register(PO6030K_BANK_C, 0x1B, 0x43);
	po6030k_write_register(PO6030K_BANK_C, 0x1D, 0x50);
	po6030k_write_register(PO6030K_BANK_C, 0x1F, 0x50);
	
	// PCLK rate according to datasheet p69
	debug_print("PO6030K PCLK config (QVGA mono):\r\n", NULL);
	po6030k_write_register(PO6030K_BANK_B, 0x68, 0x03);		
	
	// grayscale settings according to datasheet p68
	debug_print("PO6030K color config (mono):\r\n", NULL);
	po6030k_write_register(PO6030K_BANK_B, 0x38, 0x0D);
	po6030k_write_register(PO6030K_BANK_B, 0x91, 0x40);
	po6030k_write_register(PO6030K_BANK_B, 0x92, 0x01);
	po6030k_write_register(PO6030K_BANK_B, 0x93, 0xFE);
	
	if(testmode)
	{
		debug_print("Enabling test image:\r\n", NULL);
		// set camera to generate test image
		po6030k_write_register(PO6030K_BANK_B, 0x40, 0x07);
	}
}
