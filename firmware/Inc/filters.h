#ifndef _FILTERS_H
#define _FILTERS_H

#include <stdint.h>

typedef struct 
{
		uint32_t size;
		int 		 divider;
		int			 kernel[25];		// nie chce mi sie pieprzyc w dynamiczna alokacje pamieci
} filter;

extern const filter filter_mean_3;
extern const filter filter_mean_5;
extern const filter filter_gauss_3;
extern const filter filter_gauss_5;
extern const filter filter_edge;
extern const filter filter_sharpen;
extern const filter filter_sobel_x;
extern const filter filter_sobel_y;

#endif
