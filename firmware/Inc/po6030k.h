#ifndef __PO6030K_H
#define __PO6030K_H



#include <stdint.h>
#include <stdbool.h>
#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"

#define PO6030K_SLAVE_ADDR 		0xDC
#define PO6030K_REG_VAL_ID		0x6030

#define PO6030K_SKETCH_GRAY 	0
#define PO6030K_SKETCH_COLOR 	1

#define 	ARRAY_ORIGIN_X			80
#define		ARRAY_ORIGIN_Y 			8

#define		ARRAY_WIDTH					640		
#define		ARRAY_HEIGHT				480

#define 	DEVICE_ID PO6030K_DEVICE_ID
#define 	BANK_REGISTER 			0x3

#define 	IRQ_PIX_LAT 1

#define PO6030K_MODE_GRAY			0
#define PO6030K_MODE_RGB565		1
#define PO6030K_MODE_YUV			2

#define PO6030K_RES_VGA 			0x20
#define PO6030K_RES_QVGA 			0x40
#define PO6030K_RES_QQVGA 		0x80

#define PO6030K_BAYER_CLOCK_1 0x10
#define PO6030K_BAYER_CLOCK_2 0x50
#define PO6030K_BAYER_CLOCK_4 0x90
#define PO6030K_BAYER_CLOCK_8 0xB0

#define PO6030K_BANK_A 				0x0
#define PO6030K_BANK_B 				0x1
#define PO6030K_BANK_C 				0x2
#define PO6030K_BANK_D 				0x3

#define PO6030K_REG_ADDR_ID_H 0x00
#define PO6030K_REG_ADDR_ID_L 0x01
#define PO6030K_REG_REVISION	0x02
#define PO6030K_REG_BANK			0x03

#define PO6030K_SPEED_1 			PO6030K_BAYER_CLOCK_1
#define PO6030K_SPEED_2 			PO6030K_BAYER_CLOCK_2
#define PO6030K_SPEED_4 			PO6030K_BAYER_CLOCK_4
#define PO6030K_SPEED_8 			PO6030K_BAYER_CLOCK_8

#define po6030k_set_speed(div) po6030k_set_bayer_clkdiv(div)



void 			po6030k_assign_i2c(I2C_HandleTypeDef *hi2c);
uint16_t 	po6030k_get_ID(void);
void 			po6030k_set_bank(uint8_t bank);
void 			po6030k_write_register(uint8_t bank, uint8_t reg, uint8_t value);
uint8_t 	po6030k_read_register(uint8_t bank, uint8_t reg);
uint8_t 	po6030k_quick_read(uint8_t reg);
void 			po6030k_set_bayer_clkdiv(uint8_t div);
void 			po6030k_set_pclkdiv(uint8_t div);
void 			po6030k_init_cam(bool testmode);

#endif /* __PO6030K_H */
