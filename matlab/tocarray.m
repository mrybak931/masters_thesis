
function [] = tocarray(img_arr, filename)
%TOCARRAY Summary of this function goes here
%   Detailed explanation goes here

img_size = size(img_arr);
img_width = img_size(2);
img_height = img_size(1);
bytes_to_fill = 4096 - mod(img_width*img_height, 4096);

% generate header (.h) file
f_header = fopen(strcat(filename, '.h'), 'wt');
fprintf(f_header, strcat('#ifndef _', upper(filename), '_H\n'));
fprintf(f_header, strcat('#define _', upper(filename), '_H\n'));
fprintf(f_header, '\n');
fprintf(f_header, '#include <stdint.h>\n');
fprintf(f_header, '\n');
fprintf(f_header, strcat('#define', 32, filename, '_size', 32, 32, 32, num2str(img_width*img_height + bytes_to_fill), '\n'));
fprintf(f_header, strcat('#define', 32, filename, '_width', 32, 32, num2str(img_width), '\n'));
fprintf(f_header, strcat('#define', 32, filename, '_heigth', 32, num2str(img_height), '\n'));
fprintf(f_header, '\n');
fprintf(f_header, strcat('extern const uint8_t', 32, filename, '[];\n'));   % 32 - kod ASCII spacji
fprintf(f_header, '\n');
fprintf(f_header, '#endif\n');

% generate source (.c) file
f_source = fopen(strcat(filename, '.c'), 'wt');
fprintf(f_source, strcat('#include "', filename, '.h"\n'));
fprintf(f_source, '\n');
fprintf(f_source, strcat('const uint8_t', 32, filename, '[', num2str(img_width*img_height + bytes_to_fill),'] = {'));

for row = 1:img_height
    fprintf(f_source, '\n\t');
    for col = 1:img_width
        fprintf(f_source, '0x%02X, ', img_arr(row, col));
    end
    
end

for row = 1:(bytes_to_fill/256)
    fprintf(f_source, '\n\t');
    for col = 1:256
        fprintf(f_source, '0x00');
        if(row*col ~= bytes_to_fill)
            fprintf(f_source, ', ');
        end;
    end
end

fprintf(f_source, '\n};\n');

fclose('all');

end

