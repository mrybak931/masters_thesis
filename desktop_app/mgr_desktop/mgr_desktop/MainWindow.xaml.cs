﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mgr_desktop
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // deklaracja obiektów
        static SerialPort serialPort = null;
        static String serialPortName = null;
        static Timer pingTimer;

        // konstruktor okna
        public MainWindow()
        {
            InitializeComponent();
        }

        // zdarzenie po załadowaniu okna
        void OnLoad(object sender, RoutedEventArgs e)
        {
            ScanComPorts();
        }

        // callback przycisku "Connect"
        private void Button_connect_Click(object sender, RoutedEventArgs e)
        {
            if (serialPort == null)
            {
                ScanComPorts();

                if (serialPortName != null)
                {
                    TextBlock_debug.Text += "Połączono z urządzeniem.\n";
                    Scroller.ScrollToEnd();
                    serialPort = new SerialPort(serialPortName, 9600, Parity.None, 8, StopBits.One);
                }
                else
                {
                    MessageBox.Show("Nie znaleziono portu COM odpowiadającego urządzeniu. Sprawdź połączenie.", "Nie znaleziono urządzenia");
                }

                // otwórz port com
                serialPort.ReadTimeout = 1000;
                serialPort.WriteTimeout = 1000;
                serialPort.Open();

                // zacznij pingować
                StartPingTimer();

                Button_connect.Content = "Rozłącz";
            }
            else
            {
                serialPort.Close();
                serialPort = null;
                StopPingTimer();
                TextBlock_debug.Text += "Rozłączono od urządzenia.\n";
                Scroller.ScrollToEnd();
                Button_connect.Content = "Połącz";
            }
        }

        // skanowanie portów COM
        private static void ScanComPorts()
        {
            Console.WriteLine("Available Ports:");

            // co byśmy zrobili bez stackoverflow eh
            using (var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PnPEntity WHERE Caption like '%(COM%'"))
            {
                var portnames = SerialPort.GetPortNames();
                var ports = searcher.Get().Cast<ManagementBaseObject>().ToList().Select(p => p["Caption"].ToString());

                var portList = portnames.Select(n => n + " - " + ports.FirstOrDefault(s => s.Contains(n))).ToList();

                for (int i = 0; i < portList.Count; i++)
                {
                    string serialPortFriendlyName = portList.ElementAt(i);
                    Console.WriteLine("   " + serialPortFriendlyName);
                    if (serialPortFriendlyName.Contains("STMicroelectronics Virtual COM Port"))
                    {
                        serialPortName = portnames.ElementAt(i);
                    }
                }
            }
        }

        // uruchomienie wątku pingowania urządzenia co 1s
        private static void StartPingTimer()
        {
            pingTimer = new Timer(1000);
            pingTimer.Elapsed += PingTimerEvent;
            pingTimer.AutoReset = true;
            pingTimer.Enabled = true;
        }

        // zatrzymanie wątku pingowania
        private static void StopPingTimer()
        {
            pingTimer.Stop();
            pingTimer = null;
        }

        // callback timera
        private static void PingTimerEvent(Object source, ElapsedEventArgs e) {
            //Console.WriteLine("ping pong!");
            serialPort.Write("PING");
            try
            {
                String response = serialPort.ReadLine();
                if (response.Contains("PING OK"))
                    Console.WriteLine("Device ping ok.");
                else
                    NoPingHandler();
            }
            catch (TimeoutException) {
                NoPingHandler();
            }
        }

        // device unresponsive handler
        private static void NoPingHandler()
        {
            // do zrobienia potem
            MessageBox.Show("Się zepsuło :c");
            StopPingTimer();
        }

        // callback przycisku "Test łącza"
        private void Button_bandwith_test_Click(object sender, RoutedEventArgs e)
        {
            StopPingTimer();
            TextBlock_debug.Text += "Rozpoczynam test łącza.\n";
            Scroller.ScrollToEnd();
            serialPort.Write("CMD:TESTBAND\0");             // null-terminated string

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            int count = 0;
            while(count < 65536)                        // pakiet testowy - 16 transmisji po 4 kB
            {
                string response = serialPort.ReadExisting();
                if (response.Length > 0)
                {
                    Console.WriteLine(response);
                    count += response.Length;
                }
            }

            stopwatch.Stop();
            long testTime = stopwatch.ElapsedMilliseconds;
            Console.WriteLine("Test trwał " + testTime + " ms");

            TextBlock_debug.Text += "Zakończono test łącza.\n";
            TextBlock_debug.Text += "Odebrano 64 kB w czasie " + testTime + " ms\n";
            TextBlock_debug.Text += "Szybkość transferu wynosi " + (1000/testTime)*64 + " kB/s\n";
            Scroller.ScrollToEnd();
            StartPingTimer();
        }

        private void Button_snapshot_test_Click(object sender, RoutedEventArgs e)
        {
            StopPingTimer();
            TextBlock_debug.Text += "Odbieram testowy obraz.\n";
            Scroller.ScrollToEnd();
            serialPort.Write("CMD:TESTIMG\0");             // null-terminated string
            byte[] image = CaptureFrame();
            DrawImage(image);
            TextBlock_debug.Text += "Odebrano!\n";
            Scroller.ScrollToEnd();
            StartPingTimer();        
        }

        private byte[] CaptureFrame()
        { 
            byte[] imageBuffer = new byte[77824];

            int count = 0;
            while (count < 77824)
            {
                int bytesToRead = serialPort.BytesToRead;
                if (bytesToRead > 0)
                {
                    //Console.WriteLine("Do odczytu " + bytesToRead + " bajtów");
                    serialPort.Read(imageBuffer, count, bytesToRead);
                    count += bytesToRead;
                    //Console.WriteLine("Odebrano w sumie " + count + " bajtów");
                }
            }

            return imageBuffer;
        }

        private void DrawImage(Array imageArray)
        {
            // rysowanie w oknie
            BitmapSource bitmapSource = BitmapSource.Create(320, 240, 300, 300, PixelFormats.Indexed8, BitmapPalettes.Gray256, imageArray, 320);
            Image_frame.Source = bitmapSource;
        }

        private void Button_filter_test_Click(object sender, RoutedEventArgs e)
        {
            int filterIndex = Combobox_filter_test.SelectedIndex + 1;
            int algoIndex = Combobox_algo_test.SelectedIndex + 1;
            string filterName = Combobox_filter_test.Text;
            string algoName = Combobox_algo_test.Text;
            Console.WriteLine("Wybrano filtr numer " + filterIndex + " - " + filterName);
            Console.WriteLine("Wybrano algorytm numer " + algoIndex + " - " + algoName);

            StopPingTimer();
            TextBlock_debug.Text += "Testuję " + Char.ToLowerInvariant(filterName[0]) + filterName.Substring(1) + ".\n";
            Scroller.ScrollToEnd();
            serialPort.Write("CMD:TESTFILT" + filterIndex + algoIndex+ "\0");             // null-terminated string
            string response = serialPort.ReadLine();
            byte[] image = CaptureFrame();
            DrawImage(image);

            // calculate time taken
            Console.Out.WriteLine(response);
            double cyclesPerSecond = 216000000;
            double cyclesTaken = double.Parse(response.Substring(5));
            double timeTakenMs = ((cyclesTaken / cyclesPerSecond)*1000);

            TextBlock_debug.Text += "Odebrano odfiltrowany obraz.\n";
            if(timeTakenMs.Equals(0.0))
                TextBlock_debug.Text += "Pomiar czasu tylko w trybie debugu MCU.\n";
            else
                TextBlock_debug.Text += "Filtracja zajęła " + timeTakenMs + "ms.\n";
            Scroller.ScrollToEnd();
            StartPingTimer();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StopPingTimer();
            TextBlock_debug.Text += "Testuję mnożenie vs przesunięcie bitowe.\n";
            Scroller.ScrollToEnd();
            serialPort.Write("CMD:TESTMULT\0");             // null-terminated string

            // calculate time taken
            string response1 = serialPort.ReadLine();
            string response2 = serialPort.ReadLine();
            Console.Out.WriteLine(response1);
            Console.Out.WriteLine(response2);
            double cyclesPerSecond = 216000000;
            double cyclesTaken1 = double.Parse(response1.Substring(5));
            double cyclesTaken2 = double.Parse(response2.Substring(5));
            double timeTakenMs1 = ((cyclesTaken1 / cyclesPerSecond) * 1000);
            double timeTakenMs2 = ((cyclesTaken2 / cyclesPerSecond) * 1000);

            TextBlock_debug.Text += "Zakończono test.\n";
            if (timeTakenMs1.Equals(0.0) || timeTakenMs2.Equals(0.0))
                TextBlock_debug.Text += "Wystąpił błąd podczas pomiaru.\n";
            else
            {
                TextBlock_debug.Text += "Mnożenie: " + timeTakenMs1 + "ms.\n";
                TextBlock_debug.Text += "Przesunięcie: " + timeTakenMs2 + "ms.\n";
            }
            Scroller.ScrollToEnd();
            StartPingTimer();
        }

        private void Button_camera_test_Click(object sender, RoutedEventArgs e)
        {
            StopPingTimer();
            TextBlock_debug.Text += "Odbieram obraz z kamery.\n";
            Scroller.ScrollToEnd();
            serialPort.Write("CMD:TESTSNAP\0");             // null-terminated string
            byte[] image = CaptureFrame();
            DrawImage(image);
            TextBlock_debug.Text += "Odebrano!\n";
            Scroller.ScrollToEnd();
            StartPingTimer();
        }
    }
}
