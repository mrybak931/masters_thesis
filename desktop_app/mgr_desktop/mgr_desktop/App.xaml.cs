﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace mgr_desktop
{
    /// <summary>
    /// Logika interakcji dla klasy App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MessageBox.Show("Analiza wydajności mikrokontrolera z rodziny STM32F7 do przetwarzania obrazów\nAutor: Marcin Rybak", "Praca magisterska");
        }
    }
}
