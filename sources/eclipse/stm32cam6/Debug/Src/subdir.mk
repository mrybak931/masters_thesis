################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/dcmi.c \
../Src/dma.c \
../Src/dma2d.c \
../Src/fmc.c \
../Src/gpio.c \
../Src/i2c.c \
../Src/main.c \
../Src/stm32f7xx_hal_msp.c \
../Src/stm32f7xx_hal_timebase_TIM.c \
../Src/stm32f7xx_it.c \
../Src/system_stm32f7xx.c \
../Src/usart.c \
../Src/usb_device.c \
../Src/usbd_conf.c \
../Src/usbd_desc.c 

OBJS += \
./Src/dcmi.o \
./Src/dma.o \
./Src/dma2d.o \
./Src/fmc.o \
./Src/gpio.o \
./Src/i2c.o \
./Src/main.o \
./Src/stm32f7xx_hal_msp.o \
./Src/stm32f7xx_hal_timebase_TIM.o \
./Src/stm32f7xx_it.o \
./Src/system_stm32f7xx.o \
./Src/usart.o \
./Src/usb_device.o \
./Src/usbd_conf.o \
./Src/usbd_desc.o 

C_DEPS += \
./Src/dcmi.d \
./Src/dma.d \
./Src/dma2d.d \
./Src/fmc.d \
./Src/gpio.d \
./Src/i2c.d \
./Src/main.d \
./Src/stm32f7xx_hal_msp.d \
./Src/stm32f7xx_hal_timebase_TIM.d \
./Src/stm32f7xx_it.d \
./Src/system_stm32f7xx.d \
./Src/usart.d \
./Src/usb_device.d \
./Src/usbd_conf.d \
./Src/usbd_desc.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -DUSE_HAL_DRIVER -DSTM32F746xx '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -I"/home/adam/workspace/stm32cam6/Inc" -I"/home/adam/workspace/stm32cam6/Drivers/STM32F7xx_HAL_Driver/Inc" -I"/home/adam/workspace/stm32cam6/Drivers/STM32F7xx_HAL_Driver/Inc/Legacy" -I"/home/adam/workspace/stm32cam6/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/home/adam/workspace/stm32cam6/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" -I"/home/adam/workspace/stm32cam6/Drivers/CMSIS/Device/ST/STM32F7xx/Include" -I"/home/adam/workspace/stm32cam6/Drivers/CMSIS/Include" -I"/home/adam/workspace/stm32cam6/Inc"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


