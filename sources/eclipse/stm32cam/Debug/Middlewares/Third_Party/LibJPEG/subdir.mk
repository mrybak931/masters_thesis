################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Middlewares/Third_Party/LibJPEG/jmemdosa.asm 

OBJS += \
./Middlewares/Third_Party/LibJPEG/jmemdosa.o 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/Third_Party/LibJPEG/%.o: ../Middlewares/Third_Party/LibJPEG/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo $(PWD)
	arm-none-eabi-as -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -I"/home/adam/workspace/egl_etr_hal_lib" -I"/home/adam/workspace/stm32cam/inc" -I"/home/adam/workspace/egl_etr_hal_lib/HAL_Driver/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/HAL_Driver/Inc/Legacy" -I"/home/adam/workspace/egl_etr_hal_lib/CMSIS/core" -I"/home/adam/workspace/egl_etr_hal_lib/CMSIS/device" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/AUDIO/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/Template/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/DFU/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/home/adam/workspace/stm32cam/Middlewares/Third_Party/LibJPEG/include" -I"/home/adam/workspace/stm32cam/Middlewares/Third_Party/FatFs/src/drivers" -I"/home/adam/workspace/stm32cam/Middlewares/Third_Party/FatFs/src" -I"/home/adam/workspace/stm32cam/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"/home/adam/workspace/stm32cam/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7" -I"/home/adam/workspace/stm32cam/Middlewares/Third_Party/FreeRTOS/Source/include" -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


