################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../startup/startup_stm32.s 

OBJS += \
./startup/startup_stm32.o 


# Each subdirectory must supply rules for building sources it contributes
startup/%.o: ../startup/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Assembler'
	@echo $(PWD)
	arm-none-eabi-as -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -I"/home/adam/workspace/egl_etr_hal_lib" -I"/home/adam/workspace/stm32cam1/inc" -I"/home/adam/workspace/egl_etr_hal_lib/HAL_Driver/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/HAL_Driver/Inc/Legacy" -I"/home/adam/workspace/egl_etr_hal_lib/CMSIS/core" -I"/home/adam/workspace/egl_etr_hal_lib/CMSIS/device" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/AUDIO/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/Template/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/DFU/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/home/adam/workspace/stm32cam1/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"/home/adam/workspace/stm32cam1/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7" -I"/home/adam/workspace/stm32cam1/Middlewares/Third_Party/FreeRTOS/Source/include" -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


