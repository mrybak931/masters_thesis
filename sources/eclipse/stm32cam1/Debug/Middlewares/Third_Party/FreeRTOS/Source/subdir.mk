################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Middlewares/Third_Party/FreeRTOS/Source/croutine.c \
../Middlewares/Third_Party/FreeRTOS/Source/event_groups.c \
../Middlewares/Third_Party/FreeRTOS/Source/list.c \
../Middlewares/Third_Party/FreeRTOS/Source/queue.c \
../Middlewares/Third_Party/FreeRTOS/Source/tasks.c \
../Middlewares/Third_Party/FreeRTOS/Source/timers.c 

OBJS += \
./Middlewares/Third_Party/FreeRTOS/Source/croutine.o \
./Middlewares/Third_Party/FreeRTOS/Source/event_groups.o \
./Middlewares/Third_Party/FreeRTOS/Source/list.o \
./Middlewares/Third_Party/FreeRTOS/Source/queue.o \
./Middlewares/Third_Party/FreeRTOS/Source/tasks.o \
./Middlewares/Third_Party/FreeRTOS/Source/timers.o 

C_DEPS += \
./Middlewares/Third_Party/FreeRTOS/Source/croutine.d \
./Middlewares/Third_Party/FreeRTOS/Source/event_groups.d \
./Middlewares/Third_Party/FreeRTOS/Source/list.d \
./Middlewares/Third_Party/FreeRTOS/Source/queue.d \
./Middlewares/Third_Party/FreeRTOS/Source/tasks.d \
./Middlewares/Third_Party/FreeRTOS/Source/timers.d 


# Each subdirectory must supply rules for building sources it contributes
Middlewares/Third_Party/FreeRTOS/Source/%.o: ../Middlewares/Third_Party/FreeRTOS/Source/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m7 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -DSTM32F746NEHx -DSTM32F7 -DSTM32 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F746xx -DUSE_RTOS_SYSTICK -I"/home/adam/workspace/egl_etr_hal_lib" -I"/home/adam/workspace/stm32cam1/inc" -I"/home/adam/workspace/egl_etr_hal_lib/HAL_Driver/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/HAL_Driver/Inc/Legacy" -I"/home/adam/workspace/egl_etr_hal_lib/CMSIS/core" -I"/home/adam/workspace/egl_etr_hal_lib/CMSIS/device" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/AUDIO/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/HID/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/MSC/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/Template/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/CustomHID/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Class/DFU/Inc" -I"/home/adam/workspace/egl_etr_hal_lib/Middlewares/ST/STM32_USB_Device_Library/Core/Inc" -I"/home/adam/workspace/stm32cam1/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS" -I"/home/adam/workspace/stm32cam1/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM7" -I"/home/adam/workspace/stm32cam1/Middlewares/Third_Party/FreeRTOS/Source/include" -O0 -g3 -Wall -fmessage-length=0 -ffunction-sections -c -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


